package com.creditos.principal.controlador;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.creditos.principal.modelo.PlazoMod;
import com.creditos.principal.servicio.PlazoServiciosImpl;

@RestController
@RequestMapping("/api/plazos")
@Validated
public class PlazoControlador {
	
	@Autowired
	PlazoServiciosImpl plazoServicios;
	
	@PostMapping("/crearPlazo")
	public ResponseEntity<?> crear(@Valid @RequestBody PlazoMod plazoM){
		return plazoServicios.guardarPlazo(plazoM);
	}
	
	@GetMapping("/mostrarPlazos")
	public ResponseEntity<?> buscarPlazos(){
		return plazoServicios.buscarPlazos();
	}
	
	@GetMapping("/verPlazo/{nombrePlazo}")
	public ResponseEntity<?>  buscarPlazo(@Valid @PathVariable String nombrePlazo) {
		return plazoServicios.buscarNombre(nombrePlazo);
	}

	@DeleteMapping("eliminarPlazo")
	public ResponseEntity<?> eliminarPlazo(@Valid @RequestParam String id){
		return plazoServicios.eliminarPlazo(id);
	}

	@GetMapping("/cotizacio")
	public ResponseEntity<?> cotizacion(@RequestParam String idProducto, @RequestParam String idPlazo){
		return plazoServicios.cotizacion(idProducto,idPlazo);
	}

}
