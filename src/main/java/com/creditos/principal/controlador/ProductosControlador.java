package com.creditos.principal.controlador;

import com.creditos.principal.servicio.ProductosServiciosImpl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.creditos.principal.modelo.ProductosMod;

@RestController
@RequestMapping("/api/creditos")
@Validated
public class ProductosControlador {

	@Autowired
	ProductosServiciosImpl productosServicio;

	@PostMapping("/guardarProduc")
	public ResponseEntity<?> guardar(@Valid @RequestBody ProductosMod prodM) {
		return productosServicio.guardar(prodM);
	}

	@GetMapping("/buscarPorId")
	public ResponseEntity<?> buscarId(@Valid @RequestParam String id) {
		return productosServicio.buscarPorId(id);
	}

	@GetMapping("/buscarSku/{sku}")
	public ResponseEntity<?> buscarSku(@Valid @PathVariable String sku) {
		return productosServicio.buscarSku(sku);
	}

	@GetMapping("/buscarNombre/{nombre}")
	public ResponseEntity<?> buscarNombre(@Valid @PathVariable String nombre) {
		return productosServicio.buscarNombre(nombre);
	}

	@PutMapping("/actualizar")
	public ResponseEntity<?> actualizar(@Valid @RequestBody ProductosMod prodM) {
		return productosServicio.actualizar(prodM);
	}
	
	@DeleteMapping("/eliminar")
	public ResponseEntity<?> eliminar(@Valid @RequestParam String id){
		return productosServicio.eliminar(id);
	}

}
