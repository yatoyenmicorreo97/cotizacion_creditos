package com.creditos.principal.modelo;


public class CotizacionMod {

    private String nombrePlazo;

    private String nombreProducto;

    private Double precioProd;

    private int noSemanas;

    private Double tasaNormal;

    private Double tasaPuntual;

    public CotizacionMod() {
        super();
    }

    public CotizacionMod(String nombrePlazo, String nombreProducto, Double precioProd, Double tasaNormal, Double tasaPuntual, int noSemanas) {
        this.nombrePlazo = nombrePlazo;
        this.nombreProducto = nombreProducto;
        this.precioProd = precioProd;
        this.tasaNormal = tasaNormal;
        this.tasaPuntual = tasaPuntual;
        this.noSemanas= noSemanas;
    }

    public String getNombrePlazo() {
        return nombrePlazo;
    }

    public void setNombrePlazo(String nombrePlazo) {
        this.nombrePlazo = nombrePlazo;
    }

    public Double getTasaNormal() {
        return tasaNormal;
    }

    public void setTasaNormal(Double tasaNormal) {
        this.tasaNormal = tasaNormal;
    }

    public Double getTasaPuntual() {
        return tasaPuntual;
    }

    public void setTasaPuntual(Double tasaPuntual) {
        this.tasaPuntual = tasaPuntual;
    }

    public Double getPrecioProd() {
        return precioProd;
    }

    public void setPrecioProd(Double precioProd) {
        this.precioProd = precioProd;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public int getNoSemanas() {
        return noSemanas;
    }

    public void setNoSemanas(int noSemanas) {
        this.noSemanas = noSemanas;
    }
}
