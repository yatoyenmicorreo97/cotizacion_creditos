package com.creditos.principal.modelo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.*;

@Document(collection= "plazos")
public class PlazoMod {
	
	@Id
	private String id;
	
	@Field(name= "nombrePlazo")
	@NotEmpty(message = "El nombre del plazo es obligatorio")
	@Size(min = 3, max = 20, message =  "El nombre del plazo puede contener entre 3 y 20 caracteres")
	private String nombrePlazo;
	
	@Field(name = "noSemanas")
	@NotNull(message= "El numero de semanas es obligatorio")
	@Min(message = "El numero minimo de semanas es 6", value = 6)
	@Max(message = "El numero maximo de semanas es 158", value = 158)
	private int noSemanas;
	
	@Field(name= "tasaNormal")
	@NotNull(message = "La tasa normal es obligatoria")
	@Min(value = 1, message = "La menor tasa es de 1")
	@Max(value = 10, message = "La mayor tasa es de 10")
	private Double tasaNormal;

	@NotNull(message = "La tasa normal es obligatoria")
	@Min(value = 0, message = "La menor tasa es de 0")
	@Max(value = 10, message = "La mayor tasa es de 10")
	@Field(name= "tasaPuntual")
	private Double tasaPuntual;

	public PlazoMod() {
		super();
	}

	public PlazoMod(String id, String nombrePlazo, int noSemanas, Double tasaNormal, Double tasaPuntual) {
		super();
		this.id = id;
		this.nombrePlazo = nombrePlazo;
		this.noSemanas = noSemanas;
		this.tasaNormal = tasaNormal;
		this.tasaPuntual = tasaPuntual;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombrePlazo() {
		return nombrePlazo;
	}

	public void setNombrePlazo(String nombrePlazo) {
		this.nombrePlazo = nombrePlazo;
	}

	public int getNoSemanas() {
		return noSemanas;
	}

	public void setNoSemanas(int noSemanas) {
		this.noSemanas = noSemanas;
	}

	public Double getTasaNormal() {
		return tasaNormal;
	}

	public void setTasaNormal(Double tasaNormal) {
		this.tasaNormal = tasaNormal;
	}

	public Double getTasaPuntual() {
		return tasaPuntual;
	}

	public void setTasaPuntual(Double tasaPuntual) {
		this.tasaPuntual = tasaPuntual;
	}
	
	
	
	
	
	
	

	
}
