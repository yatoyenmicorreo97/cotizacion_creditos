package com.creditos.principal.modelo;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "productos")
public class ProductosMod {
	
	@Id
	private String id;
	
	@Field(name = "sku")
	@NotEmpty(message = "El campo SKU no puede ser null o vacio")
	@Size(min = 3, max = 20 , message = "El codigo del producto tiene entre 3 y 20 caracteres")
	@Pattern(regexp = "[A-Z0-9]+", message ="El sku del producto solo puede tenerletras mayusculas o numeros")
	private String sku;
	
	@Field(name = "nombre")
	@NotEmpty(message = "El campo Nombre no puede ser null o vacio")
	private String nombre;
	
	@Field(name = "categoria")
	@NotEmpty(message = "El campo Categoria no puede ser null o vacio")
	@Pattern(regexp = "[A-Z0-9]+",message = "Deben de ser letras mayusculas")
	private String categoria;
	
	@Field(name = "precio")
	@NotNull(message = "El Precio es obligatorio")
	@Min(message = "El preciominimo debe de ser 0",value = 0)
	private Double precio;
	
	public ProductosMod() {
		super();
	}

	public ProductosMod(String id_Prod, String sku, String nombre, String categoria, Double precio) {
		super();
		this.id = id_Prod;
		this.sku = sku;
		this.nombre = nombre;
		this.categoria = categoria;
		this.precio = precio;
	}

	public String getId_Prod() {
		return id;
	}

	public void setId_Prod(String id_Prod) {
		this.id = id_Prod;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
}
