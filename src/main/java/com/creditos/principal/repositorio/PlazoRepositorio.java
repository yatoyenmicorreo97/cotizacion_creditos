package com.creditos.principal.repositorio;

import java.util.ArrayList;
import java.util.List;

import com.creditos.principal.modelo.CotizacionMod;
import com.creditos.principal.modelo.ProductosMod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.creditos.principal.modelo.PlazoMod;


@Repository
public class PlazoRepositorio {
	@Autowired
	MongoTemplate mongoTemplate;
	
	public void guardarPlazo(PlazoMod plazoM) {
		mongoTemplate.save(plazoM);
	}
	
	public List<PlazoMod> nombrePlazo(Query query){
		return mongoTemplate.find(query, PlazoMod.class);
	}
	
	public List<PlazoMod> lista(){
		return mongoTemplate.findAll(PlazoMod.class);
	}
	
	public void eliminarPlazo(Query query) {
		mongoTemplate.remove(query, PlazoMod.class);
	}

	public PlazoMod buscarPlazo(String id){
		return mongoTemplate.findById(id, PlazoMod.class);
	}

}
