package com.creditos.principal.repositorio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.creditos.principal.modelo.ProductosMod;


@Repository
public class ProductosRepositorio {
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	public void  guardarProd(ProductosMod prodM) {
		mongoTemplate.save(prodM);
	}

	public ProductosMod buscarPorId(String id){
		return mongoTemplate.findById(id,ProductosMod.class);
	}
	
	public List<ProductosMod> lista(Query query){
		return mongoTemplate.find(query, ProductosMod.class);
	}
	
	public List<ProductosMod> nombreProd(Query query){
		return mongoTemplate.find(query, ProductosMod.class);
	}
	
	public void actualizar(Update update, Query query) {
		mongoTemplate.updateFirst(query, update, ProductosMod.class);
	}
	
	public void eliminar( Query query){
		mongoTemplate.remove(query, ProductosMod.class);
	}
	
}
