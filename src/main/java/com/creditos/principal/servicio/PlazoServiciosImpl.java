package com.creditos.principal.servicio;

import com.creditos.principal.modelo.CotizacionMod;
import com.creditos.principal.modelo.ProductosMod;
import com.creditos.principal.repositorio.ProductosRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.creditos.principal.modelo.PlazoMod;
import com.creditos.principal.repositorio.PlazoRepositorio;

@Service
public class PlazoServiciosImpl {
	
	@Autowired
	PlazoRepositorio plazoRepositorio;
	@Autowired
	ProductosRepositorio productosRepositorio;
	
	public ResponseEntity<?> guardarPlazo(PlazoMod plazoM){
		try{
			plazoRepositorio.guardarPlazo(plazoM);
			return new ResponseEntity<>(HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	public ResponseEntity<?> buscarPlazos() {
		try{
			return new ResponseEntity<>(plazoRepositorio.lista(), HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	public ResponseEntity<?> buscarNombre(String nombrePlazo) {
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("nombrePlazo").is(nombrePlazo));
			return new ResponseEntity<>(plazoRepositorio.nombrePlazo(query), HttpStatus.OK);
		}catch (Exception e){
				return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
	}

	public ResponseEntity<?> eliminarPlazo(String id){
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("id").is(id));
			plazoRepositorio.eliminarPlazo(query);
			return new ResponseEntity<>(HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public ResponseEntity<?> cotizacion(String idProd, String idPlazo){
		try {
			ProductosMod prodM = productosRepositorio.buscarPorId(idProd);
			PlazoMod plazoM = plazoRepositorio.buscarPlazo(idPlazo);

			CotizacionMod cotizacion = new CotizacionMod();

			Double tasaNormal = ((prodM.getPrecio() * plazoM.getTasaNormal()) + prodM.getPrecio()) / plazoM.getNoSemanas();
			Double tasaPuntual = ((prodM.getPrecio() * plazoM.getTasaPuntual()) + prodM.getPrecio()) / plazoM.getNoSemanas();

			cotizacion.setNombrePlazo(plazoM.getNombrePlazo());
			cotizacion.setNombreProducto(prodM.getNombre());
			cotizacion.setPrecioProd(prodM.getPrecio());
			cotizacion.setNoSemanas(plazoM.getNoSemanas());
			cotizacion.setTasaNormal(tasaNormal);
			cotizacion.setTasaPuntual(tasaPuntual);
			return new ResponseEntity<>(cotizacion, HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
