package com.creditos.principal.servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.creditos.principal.modelo.ProductosMod;
import com.creditos.principal.repositorio.ProductosRepositorio;

@Service
public class ProductosServiciosImpl {

	@Autowired
	ProductosRepositorio productosRepositorio;

	public ResponseEntity<?> guardar(ProductosMod prodM) {
		try {
			productosRepositorio.guardarProd(prodM);
			return new ResponseEntity<>(HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public ResponseEntity<?> buscarPorId(String id) {
		try {
			ProductosMod mod = productosRepositorio.buscarPorId(id);
			return new ResponseEntity<>(mod, HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public ResponseEntity<?> buscarSku(String sku) {
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("sku").is(sku));
			return new ResponseEntity<>(productosRepositorio.lista(query), HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public ResponseEntity<?> buscarNombre(String nombre) {
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("nombre").is(nombre));
			return new ResponseEntity<>(productosRepositorio.nombreProd(query), HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public ResponseEntity<?> actualizar(ProductosMod prodM) {
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("id").is(prodM.getId_Prod()));
			Update update = new Update();
			update.set("sku", prodM.getSku());
			update.set("nombre", prodM.getNombre());
			update.set("categoria", prodM.getCategoria());
			update.set("precio", prodM.getPrecio());

			productosRepositorio.actualizar(update, query);

			return new ResponseEntity<>(HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public ResponseEntity<?> eliminar(String id){
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("id").is(id));

			productosRepositorio.eliminar(query);

			return new ResponseEntity<>(HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
